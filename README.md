## À propos du Projet
Ce projet a été développé spécifiquement pour les 
entretiens techniques chez Utech pour les postes 
impliquant le framework Symfony. Il vise à évaluer 
les compétences des candidats en matière de développement 
web avec PHP et Symfony, en se concentrant sur des 
aspects tels que la conception d'APIs, le mappage de données, 
et l'application de logiques métier complexes.

Aucune base de données n'est utilisée afin de faciliter la mise en place et l'utilisation 
de ce projet lors d'entretien technique. 

## Prérequis
- PHP: Version 8.2
- Symfony: Version 6.3
