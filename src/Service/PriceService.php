<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\Promotion;
use App\Entity\PromotionCategory;

class PriceService
{
    public function calculatePriceAfterPromotion(Product $product, Promotion $promotion): float
    {
        if ($promotion->getCategory() === PromotionCategory::PERCENTAGE) {
            $promotionPrice = $product->getPrice() * ($promotion->getRate() / 100);
            return $product->getPrice() - $promotionPrice;
        } elseif ($promotion->getCategory() === PromotionCategory::AMOUNT) {
            return $product->getPrice() - $promotion->getRate();
        } else {
            return $product->getPrice();
        }
    }
}
