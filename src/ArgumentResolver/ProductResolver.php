<?php

namespace App\ArgumentResolver;

use App\Entity\Product;
use App\Repository\Interface\ProductRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ProductResolver implements ValueResolverInterface
{
    public function __construct(private readonly ProductRepositoryInterface $productRepository)
    {
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return Product[]
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->supports($argument)) {
            return [];
        }

        return $this->apply($request);
    }

    private function supports(ArgumentMetadata $argument): bool
    {
        return $argument->getType() === Product::class;
    }

    /**
     * @param Request $request
     * @return Product[]
     */
    private function apply(Request $request): array
    {
        if (!$request->get('id') || !is_string($request->get('id'))) {
            return [];
        }

        if ($product = $this->productRepository->find($request->get('id'))) {
            return [$product];
        }

        return [];
    }
}
