<?php
namespace App\Controller\Comment\GetComments;


use App\Entity\Comment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\Interface\CommentRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route(path: '/api/comments', name: 'comments_list', methods: 'GET')]
class ApiGetCommentsController extends AbstractController
{
    public function __invoke(CommentRepositoryInterface $commentRepository): Response
    {
        return new JsonResponse(
            array_map(
                fn (Comment $comment): array => [
                    'content' => $comment->getContent(),
                    'dateSend' => $comment->getDateSend(),
                    'username' => $comment->getUsername(),
                    'product' => $comment->getProduct()->getName(),
                    'id' => $comment->getId()
                ],
                $commentRepository->findAll()
            ),
        );
    }
}
