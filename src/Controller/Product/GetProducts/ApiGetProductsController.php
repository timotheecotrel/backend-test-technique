<?php

namespace App\Controller\Product\GetProducts;

use App\Entity\Product;
use App\Repository\Interface\ProductRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/products', name: 'products_list', methods: 'GET')]
class ApiGetProductsController extends AbstractController
{
    public function __invoke(ProductRepositoryInterface $productRepository): Response
    {
        return new JsonResponse(
            array_map(
                fn (Product $product): array => [
                    'id' => $product->getId()?->toRfc4122(),
                    'name' => $product->getName(),
                    'description' => $product->getDescription(),
                    'price' => $product->getPrice(),
                    'quantity' => $product->getQuantity()
                ],
                $productRepository->findAll()
            ),
        );
    }
}
