<?php
namespace App\Controller\Product\GetProducts;

use App\Entity\Comment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\Interface\CommentRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route(path: '/api/product/{id}', name: 'product_list', methods: 'GET')]
class ApiGetOneProductCommentController extends AbstractController
{

    public function __invoke(string $id, CommentRepositoryInterface $commentRepository): Response
    {
        $commentProduct = [];
        foreach($commentRepository->findAll() as $comment){
            if($comment->getProduct()->getId()?->toRfc4122() === $id){
                $commentProduct[] = $comment;
            }
        }
        usort($commentProduct, function($a, $b){
            return $b->getDateSend() <=> $a->getDateSend();
        });
        return new JsonResponse(
            array_map(
                fn (Comment $comment): array => [
                    'Content' => $comment->getContent(),
                    'dateSend' => $comment->getDateSend()->format('d/m/Y H:i:s'),
                    'username' => $comment->getUsername()
                ],
                $commentProduct
            ),
        );
    }


   
}
