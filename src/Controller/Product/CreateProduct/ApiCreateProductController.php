<?php

namespace App\Controller\Product\CreateProduct;

use App\Entity\Product;
use App\Repository\Interface\ProductRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[Route(path: '/api/products', name: 'products_create', methods: ['POST'])]
class ApiCreateProductController extends AbstractController
{
    public function __invoke(
        #[MapRequestPayload] CreateProductCommand $createProductCommand,
        ProductRepositoryInterface $productRepository
    ): Response {
        $product = $productRepository->save(
            Product::create(
                name: $createProductCommand->getName(),
                description: $createProductCommand->getDescription(),
                quantity: $createProductCommand->getQuantity(),
                price: $createProductCommand->getPrice(),
                id: Uuid::v4()
            )
        );

        return new JsonResponse(
            [
                'id' => $product->getId()?->toRfc4122(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'quantity' => $product->getQuantity(),
                'price' => $product->getPrice()
            ],
            Response::HTTP_CREATED
        );
    }
}
