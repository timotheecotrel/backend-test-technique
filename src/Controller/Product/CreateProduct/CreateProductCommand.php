<?php

namespace App\Controller\Product\CreateProduct;

use Symfony\Component\Validator\Constraints as Assert;

class CreateProductCommand
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min: 5, max: 255)]
        private readonly string $name,
        #[Assert\NotBlank]
        #[Assert\Length(min: 5)]
        private readonly string $description,
        #[Assert\Positive]
        private readonly float $price,
        #[Assert\PositiveOrZero]
        private readonly int $quantity
    ) {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
