<?php

namespace App\Controller\Product\CalculatePromotionalPrice;

use App\Entity\Product;
use App\Entity\Promotion;
use App\Entity\PromotionCategory;
use App\Service\PriceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/product/{id}/promotions', name: 'calculatePromotionForAProduct', methods: ['GET'])]
class ApiCalculatePromotionalePrice extends AbstractController
{
    public function __invoke(
        #[MapQueryString] PromotionQuery $promotionQuery,
        Product $product,
        PriceService $priceService
    ): Response {
        $promotion = Promotion::create(
            category: PromotionCategory::from($promotionQuery->getCategory()),
            rate: $promotionQuery->getRate()
        );

        $newPrice = $priceService->calculatePriceAfterPromotion(product: $product, promotion: $promotion);

        return new JsonResponse($newPrice);
    }
}
