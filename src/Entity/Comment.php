<?php

namespace App\Entity;

use DateTimeImmutable;

class Comment
{
    public function __construct(
        private ?int $id =null,
        private ?string $content=null,
        private ?\DateTimeImmutable $dateSend =null,
        private ?string $username =null,
        private ?Product $product =null
    )
    {
        if(!$this->id){
            $this->id = rand(1, 20);
        }
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getDateSend(): ?\DateTimeImmutable
    {
        return $this->dateSend;
    }

    public function setDateSend(\DateTimeImmutable $dateSend): static
    {
        $this->dateSend = $dateSend;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $Product): static
    {
        $this->product = $Product;

        return $this;
    }

    public static function create(
        string $content,
        DateTimeImmutable $dateSend,
        string $username,
        int $id,
        ?Product $product = null
    ): self {
        return new self(
            content: $content,
            dateSend: $dateSend,
            username: $username,
            id: $id,
            product: $product
        );
    }
}
