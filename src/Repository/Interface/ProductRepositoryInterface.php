<?php

namespace App\Repository\Interface;

use App\Entity\Product;

interface ProductRepositoryInterface
{
    public function find(string $id): ?Product;

    /**
     * @return Product[]
     */
    public function findAll(): array;
    public function save(Product $product): Product;
}
