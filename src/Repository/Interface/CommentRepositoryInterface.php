<?php

namespace App\Repository\Interface;

use App\Entity\Comment;

interface CommentRepositoryInterface
{
    public function find(string $id): ?Comment;

    /**
     * @return Comment[]
     */
    public function findAll(): array;
    public function save(Comment $comment): Comment;
}
