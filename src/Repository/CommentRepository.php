<?php

namespace App\Repository;

use Faker\Factory;
use DateTimeImmutable;
use App\Entity\Comment;
use App\Entity\Product;
use App\Repository\Interface\CommentRepositoryInterface;
use App\Repository\Interface\ProductRepositoryInterface;
use DateInterval;

class CommentRepository implements CommentRepositoryInterface
{
     /** @var Comment[] $comments */
     private array $comments;

     /** @var Product[] $products */
     private array $products;


     public function __construct(ProductRepositoryInterface $productRepositoryInterface)
    {
        $this->products = $productRepositoryInterface->findAll();
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 20; $i++) {
            $comment = new Comment();
            $date = new DateTimeImmutable("now");
            $comment->setContent($faker->sentence($nbWords = 6, $variableNbWords = true));
            $comment->setDateSend($date->sub(new DateInterval("P".random_int(0, 150)."D")));
            $comment->setUsername($faker->userName);
            $this->addCommentToProduct($comment);
            $this->comments[$i] = $comment;
        }
    }

    public function find(string $id): ?Comment
    {
        return array_values(array_filter(
            $this->comments,
            fn (Comment $comment) => $comment->getId() === $id
        ))[0] ?? null;
    }

    /**
     * @return Comment[]
     */
    public function findAll(): array
    {
        return $this->comments;
    }

    public function save(Comment $comment): Comment
    {
        return $comment;
    }

    public function addCommentToProduct(Comment $comment)
    {
        $keyProduct = array_rand($this->products);
        $comment->setProduct($this->products[$keyProduct]);
    }
}
